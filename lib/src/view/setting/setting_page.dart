import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:provider/provider.dart';
import 'package:read_paper/src/model/detail_voice_read.dart';
import 'package:read_paper/src/viewmodal/setting/setting_viewmodel.dart';
import 'package:speech_to_text/speech_to_text.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  DetailVoiceRead? detailVoiceRead;
  double _currentSpeed = 0.5;
  double _currentPitch = 1;
  late StateSetter _setStateSpeech;
  late StateSetter _setStatePitch;
  SpeechToText speechToText = SpeechToText();
  FlutterTts flutterTts = FlutterTts();
  List<String> chunksSpeed = [];
  List<String> chunksPitch = [];
  String nameRead = "Giọng đọc 1";
  bool isAvailable = false;
  bool isLoading = false;
  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();

  List<Map<String, String>> voiceVNList = [
    {"key": "vi-VN-language", "locale": "vi-VN", "name": "Giọng đọc 1"},
    {"key": "vi-vn-x-gft-network", "locale": "vi-VN", "name": "Giọng đọc 2"},
    {"key": "vi-vn-x-vie-network", "locale": "vi-VN", "name": "Giọng đọc 3"},
    {"key": "vi-vn-x-vid-local", "locale": "vi-VN", "name": "Giọng đọc 4"},
    {"key": "vi-vn-x-vic-local", "locale": "vi-VN", "name": "Giọng đọc 5"},
    {"key": "vi-vn-x-gft-local", "locale": "vi-VN", "name": "Giọng đọc 6"},
    {"key": "vi-vn-x-vie-local", "locale": "vi-VN", "name": "Giọng đọc 7"},
    {"key": "vi-vn-x-vif-network", "locale": "vi-VN", "name": "Giọng đọc 8"},
    {"key": "vi-vn-x-vif-local", "locale": "vi-VN", "name": "Giọng đọc 9"},
    {"key": "vi-vn-x-vid-network", "locale": "vi-VN", "name": "Giọng đọc 10"},
    {"key": "vi-vn-x-vic-network", "locale": "vi-VN", "name": "Giọng đọc 11"}
  ];
  Map<String, String> voiceRead = {"name": "vi-VN-language", "locale": "vi-VN"};

  @override
  void initState() {
    super.initState();
    loadDataFromApi();
    initSpeech();
  }

  loadDataFromApi() {
    setState(() {
      getDetail();
    });
  }

  Future<void> getDetail() async {
    setState(() {
      isLoading = true;
    });

    try {
      final res = Provider.of<SettingViewModel>(context, listen: false);
      DetailVoiceRead? data = await res.getDetailVoidRead();
      if (data != null) {
        setState(() {
          detailVoiceRead = data;
          voiceVNList.forEach((element) {
            if (element['name'] == data.giongDoc) {
              voiceRead = {
                "name": element['key'] ?? "",
                "locale": element['locale'] ?? ""
              };
            }
          });
          controller.text = detailVoiceRead?.vanBanChayThu ?? "";
          _currentSpeed = double.parse(data.tocDo ?? "0.5");
          _currentPitch = double.parse(data.caoDo ?? "1");
          Future.delayed(Duration(milliseconds: 500), () {
            setState(() {
              isLoading = false;
            });
          });
        });
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print("Error $e");
    }
  }

  void setVoice() async {
    Map<String, dynamic> data = {
      "giongDoc": nameRead,
      "tocDo": _currentSpeed,
      "caoDo": _currentPitch,
      "vanBanChayThu": controller.text
    };
    try {
      final res = Provider.of<SettingViewModel>(context, listen: false);
      await res.setDetailVoidRead(data);
    } catch (e) {
      print("Error $e");
    }
  }

  Future<void> speak(String word) async {
    await flutterTts.setLanguage('vi-VN');
    await flutterTts.setVoice(voiceRead);
    await flutterTts.setPitch(_currentPitch);
    await flutterTts.setSpeechRate(_currentSpeed);

    flutterTts.setProgressHandler((text, start, end, word) {});

    await flutterTts.speak(word);
  }

  void initSpeech() async {
    isAvailable = await speechToText.initialize();
  }

  @override
  void dispose() {
    flutterTts.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<SettingViewModel>(context, listen: true);
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.99),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Cài đặt giọng đọc",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
        ),
        elevation: 0,
      ),
      body: GestureDetector(
        onTap: () {
          focusNode.unfocus();
        },
        child: isLoading == false
            ? buildPage(context)
            : Center(
                child: CircularProgressIndicator(
                color: Colors.grey,
                strokeWidth: 3,
              )),
      ),
    );
  }

  Widget buildPage(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
      child: Column(
        children: [
          InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              showDialog(
                context: context,
                //barrierDismissible: false,
                builder: (BuildContext context) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    insetPadding: EdgeInsets.all(10),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Chọn Giọng đọc'),
                        InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Icon(Icons.close))
                      ],
                    ),
                    content: Container(
                      width: double.maxFinite,
                      height: 500,
                      child: ListView(
                        children: voiceVNList
                            .map(
                              (item) => RadioListTile(
                                activeColor: Colors.teal,
                                contentPadding: EdgeInsets.zero,
                                title: Text(item['name'] ?? ""),
                                value: item['name'] ?? "",
                                groupValue: nameRead,
                                onChanged: (value) {
                                  setState(() {
                                    Map<String, String> voiceName = {
                                      "name": item['key']!,
                                      "locale": item['locale']!
                                    };
                                    nameRead = value.toString();
                                    detailVoiceRead?.giongDoc =
                                        value.toString();
                                    voiceRead = voiceName;
                                    setState(() {});
                                    Navigator.of(context).pop();
                                  });
                                },
                              ),
                            )
                            .toList(),
                      ),
                    ),
                  );
                },
              );
            },
            child: buildItem(
              icon: Icons.settings_voice,
              title: "Chọn giọng đọc",
              widget: Text(
                detailVoiceRead?.giongDoc ?? nameRead,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.black.withOpacity(0.6)),
              ),
            ),
          ),
          SizedBox(height: 20),
          buildItem(
            icon: Icons.speed,
            title: "Tốc độ đọc",
            widget: StatefulBuilder(
              builder: (BuildContext context,
                  void Function(void Function()) setState) {
                _setStateSpeech = setState;
                return Container(
                  margin: EdgeInsets.only(right: 20),
                  child: SliderTheme(
                    data: SliderThemeData(
                      trackShape: CustomTrackShape(),
                      thumbShape: RoundSliderThumbShape(enabledThumbRadius: 5),
                      overlayShape: RoundSliderOverlayShape(overlayRadius: 10),
                    ),
                    child: Slider(
                      activeColor: Colors.teal,
                      value: _currentSpeed,
                      min: 0.1,
                      max: 0.9,
                      divisions: 100,
                      onChanged: (double value) {},
                      onChangeEnd: (double value) {
                        _setStateSpeech(() {
                          _currentSpeed = value;
                        });
                        //speak(chunksSpeed[SpeedIndex]);
                      },
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(height: 20),
          buildItem(
            icon: Icons.record_voice_over_rounded,
            title: "Cao độ giọng đọc",
            widget: StatefulBuilder(
              builder: (BuildContext context,
                  void Function(void Function()) setState) {
                _setStatePitch = setState;
                return Container(
                  margin: EdgeInsets.only(right: 20),
                  child: SliderTheme(
                    data: SliderThemeData(
                      trackShape: CustomTrackShape(),
                      thumbShape: RoundSliderThumbShape(enabledThumbRadius: 5),
                      overlayShape: RoundSliderOverlayShape(overlayRadius: 10),
                    ),
                    child: Slider(
                      activeColor: Colors.teal,
                      value: _currentPitch,
                      min: 0.5,
                      max: 1.5,
                      divisions: 100,
                      onChanged: (double value) {},
                      onChangeEnd: (double value) {
                        _setStatePitch(() {
                          _currentPitch = value;
                        });
                        //speak(chunksPitch[PitchIndex]);
                      },
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(height: 20),
          buildItem(
            icon: Icons.sticky_note_2_outlined,
            title: "Văn bản chạy thử",
            widget: TextField(
              controller: controller,
              focusNode: focusNode,
              textInputAction: TextInputAction.done,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.black.withOpacity(0.6)),
              decoration: InputDecoration.collapsed(
                hintText: controller.text.isNotEmpty
                    ? controller.text
                    : "Xin chào, bạn có khoẻ không",
                hintStyle: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.black.withOpacity(0.6)),
                border: InputBorder.none,
              ),
              onChanged: (value) {
                controller.text = value;
                controller
                  ..selection =
                      TextSelection.collapsed(offset: controller.text.length);
              },
            ),
          ),
          SizedBox(height: 20),
          InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () async {
              speak(controller.text.isNotEmpty
                  ? controller.text
                  : "Xin chào, bạn có khoẻ không");
            },
            child: buildItem(
              icon: Icons.volume_up_rounded,
              title: "Chạy thử",
            ),
          ),
          SizedBox(height: 20),
          InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              setState(() {
                detailVoiceRead?.giongDoc = nameRead = "Giọng đọc 1";
                voiceRead = {"name": "vi-VN-language", "locale": "vi-VN"};
                _currentSpeed = 0.5;
                _currentPitch = 1;
                controller.text = "Xin chào, bạn có khoẻ không ";
              });
            },
            child: buildItem(
              icon: Icons.settings_backup_restore,
              title: "Đặt lại mặc định",
              widget: Text(
                "Tốc độ, cao độ giọng đọc và văn bản chạy thử",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.black.withOpacity(0.6)),
              ),
            ),
          ),
          SizedBox(height: 20),
          InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              setVoice();
              Navigator.pop(context);
            },
            child: buildItem(
              icon: Icons.save,
              title: "Lưu cài đặt",
            ),
          ),
        ],
      ),
    );
  }

  Widget buildItem(
      {required IconData icon, required String title, Widget? widget}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, size: 22),
        SizedBox(width: 32),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
              ),
              SizedBox(height: widget != null ? 4 : 0),
              widget ?? SizedBox(),
            ],
          ),
        )
      ],
    );
  }
}

class CustomTrackShape extends RoundedRectSliderTrackShape {
  @override
  Rect getPreferredRect({
    required RenderBox parentBox,
    Offset offset = Offset.zero,
    required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final trackWidth = parentBox.size.width;
    return Rect.fromLTWH(0, 10, trackWidth, 1);
  }
}
